<?php

include_once("Persona.php");
include_once("serializarArchivo.php");

$personaActual = new Persona();
$eventoFormulario = "insertarPersona";
$listaPersonas = serializarArchivo::deserializar();

if(isset($_POST['insertarPersona']))
{
    $nuevoPersona = new Persona((int)$_POST['id'], $_POST['nombre'], $_POST['edad'], $_POST['sexo']);

    array_push($listaPersonas, $nuevoPersona);

    $resultado = serializarArchivo::serializar($listaPersonas);

    if($resultado)
        echo "<p>Dato insertado correctamente</p>";
    else
        echo "<p>Error al insertar el dato</p>";
}


if(isset($_GET['editarPersona']))
{
    $idPersona = $_GET['editarPersona'];

    for($i = 0; $i < count($listaPersonas); $i++)
    {
        if($listaPersonas[$i]->id == $idPersona)
        {
            $personaActual = $listaPersonas[$i];
            $eventoFormulario = "actualizarPersona";
            break;
        }
    }
}

if(isset($_POST['actualizarPersona']))
{
    for($i = 0; $i < count($listaPersonas); $i++)
    {
        if($listaPersonas[$i]->id == $_POST['id'])
        {
            $listaPersonas[$i]->nombre = $_POST['nombre'];
            $listaPersonas[$i]->edad = $_POST['edad'];
            $listaPersonas[$i]->sexo = $_POST['sexo'];
            break;
        }
    }

    $resultado = serializarArchivo::serializar($listaPersonas);

    if($resultado)
        echo "<p>Dato actualizado correctamente</p>";
    else
        echo "<p>Error al actualizar el dato</p>";
}

if(isset($_POST['eliminarPersona']))
{
    for($i = 0; $i < count($listaPersonas); $i++)
    {
        if($listaPersonas[$i]->id == $_POST['id'])
        {
            unset($listaPersonas[$i]);
            $listaPersonas = array_values($listaPersonas);
            break;
        }
    }

    $resultado = serializarArchivo::serializar($listaPersonas);

    if($resultado)
        echo "<p>Dato borrado correctamente</p>";
    else
        echo "<p>Error al borrar el dato</p>";
}

?>


<h1>Módulo personas</h1>
<hr>
<a href="PersonaIndex.php">NuevoElemento</a>
<form method="POST">
        <p>Id: <input type="number" name="id" value="<?php echo $personaActual->id; ?>"
            <?php if($eventoFormulario == "actualizarPersona") echo "readonly"; ?> /></p>
            
        <p>Nombre: <input type="text" name="nombre" value="<?php echo $personaActual->nombre; ?>" /></p>
        <p>Edad: <input type="number" name="edad" value="<?php echo $personaActual->edad; ?>" /></p>
        <p>Sexo: <select name="sexo">
                    <option value="Masculino" <?php if($personaActual->sexo == "Masculino") echo "selected"; ?>>
                        Masculino
                    </option>
                    <option value="Femenino" <?php if($personaActual->sexo == "Femenino") echo "selected"; ?>>
                        Femenino
                    </option>
                </select></p>
        <p><input type="submit" name="<?php echo $eventoFormulario; ?>" value="Guardar" /></p>
</form>
<hr>

<table>
    <tr><th>Id</th><th>Nombre</th><th>Edad</th><th>Sexo</th><th>Acciones</th></tr>

<?php
    foreach($listaPersonas as $persona)
    {
        echo "<tr><td>" . $persona->id . "</td>" . 
        "<td>" . $persona->nombre . "</td>" . 
        "<td>" . $persona->edad . "</td>" . 
        "<td>" . $persona->sexo . "</td>" . 
        "<td> <a href='?editarPersona=" . $persona->id . "'>Editar</a>" . 
        "<td> <a href='?borrarPersona=" . $persona->id . "'>Borrar</a>" . 
        "</td></tr>";
    }
?>

</table>

<hr>

<?php
if(isset($_GET['borrarPersona']))
{
    $idPersona = $_GET['borrarPersona'];

    for($i = 0; $i < count($listaPersonas); $i++)
    {
        if($listaPersonas[$i]->id == $idPersona)
        {
            $personaActual = $listaPersonas[$i];
            break;
        }
    }

    ?>

    <form method="POST" action="PersonaIndex.php">
        <input type="hidden" name="id" value="<?php echo $personaActual->id; ?>" />
        <p>Nombre: <?php echo $personaActual->nombre; ?></p>
        <p>Edad: <?php echo $personaActual->edad; ?></p>
        <p>Sexo: <?php echo $personaActual->sexo; ?></p>
        <p>¿Seguro que desea eliminar el registro?</p>
        <p><input type="submit" name="eliminarPersona" value="Eliminar" /></p>
    </form>


<?php
}
?>

<br />
<a href="index.php">Regresar</a>